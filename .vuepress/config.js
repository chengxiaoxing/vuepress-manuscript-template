module.exports = {
    themeConfig: {
        displayAllHeaders: true,
        nav:[
            {text: 'Home',link:'/'},
            {text: 'Figures',
                items:[
                    {text: 'Figure 1',link:'/figure1/'},
                    {text: 'Figure 2',link:'/figure2/'}]},
            {text: 'Visual',link:'/visual/'}
        ],
        sidebar: 'auto'
    }
  }